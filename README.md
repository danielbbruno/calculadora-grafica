# Calculadora Gráfica

Esse aplicativo é uma calculadora gráfica para Android, que serve para desenhar o gráfico de funções matemáticas de uma variável.

Foi desenvolvido durante minha atuação como monitor do Curso de Programação para Smartphone no Laboratório de Informática para Educação(LIpE) da UFRJ, fazendo uso de uma das ferramentas usadas no curso, o App Inventor.

![](./misc/exemplo.png)


## Como instalar
O arquivo .apk para android está disponível [aqui](https://gitlab.com/danielbbruno/calculadora-grafica/-/tags/v1.0.0). Para instalar, é necessário permitir a instalação de aplicativos de fontes desconhecidas.

## Como utilizar
Escreva uma equação em uma das linhas de entrada e aperte o botão `Desenha` ao lado da equação para que o gráfico seja desenhado na tela. A equação de entrada deve usar apenas números racionais, a variável `x`, parênteses e as quatro operações elementares (`+`, `-`, `*`, `/`). Deve também possuir espaço entre os símbolos usados, como no exemplo abaixo:
```
( 21 + 67 * x ) / 2 - 12
```
A origem do plano cartesiano é indicada pela interseção das duas linhas brancas mais intensas no gráfico, e as demais linhas brancas indicam a variação de uma unidade.
Cada linha de entrada está associada à uma cor diferente no gráfico.
Para restaurar o gráfico para o estado inicial, aperte o botão `Limpa a tela`.

Talvez seja necessário apertar o botão `Limpa a tela` algumas vezes no ínicio da execução para que o plano cartesiano seja desenhado adequadamente.


## Funcionamento
Quando o botão `Desenha` é apertado, os seguintes passos são feitos:
- Cria-se uma lista de elementos da equação ao separar o texto por espaços, e avalia o tipo léxico de cada elemento dessa lista.
- Em posse dos tipos dos elementos da equação, o [algoritmo de triagem](https://en.wikipedia.org/wiki/Shunting-yard_algorithm) é aplicado, gerando uma lista de itens na [notação polonesa inversa](https://pt.wikipedia.org/wiki/Nota%C3%A7%C3%A3o_polonesa_inversa).
- Com a equação neste formato, esta é avaliada para cada ponto na tela, fazendo a transformação linear adequada para mover a origem para o centro da tela, e fazer com que a coordenada `y` possua seus maiores valores no topo da tela.
- Para cada par de pontos consecutivos na coordenada `x`, traça-se no gráfico uma reta que os conecta - esta solução é ingênua, e só exibe gráficos de funções cujo domínio incluem o intervalo contínuo `(-5, 5)`.


 
